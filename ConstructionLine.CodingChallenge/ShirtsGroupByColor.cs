using System.Collections.Generic;
using System.Linq;

namespace ConstructionLine.CodingChallenge
{
    public class ShirtsGroupByColor
    {
        public Color Color { get; set; }
        public IEnumerable<IGrouping<Size, Shirt>> SizeGroups { get; set; }
    }
}