﻿﻿using System.Collections.Generic;
using System.Linq;

namespace ConstructionLine.CodingChallenge
{
    public class SearchEngine
    {
        private readonly IEnumerable<ShirtsGroupByColor> _groupByColorAndSize;

        public SearchEngine(IEnumerable<Shirt> shirts)
        {
            _groupByColorAndSize = from s in shirts
                group s by s.Color
                into g
                select new ShirtsGroupByColor
                {
                    Color = g.Key,
                    SizeGroups = g.ToList().GroupBy(c => c.Size)
                };
        }

        public SearchResults Search(SearchOptions options)
        {
            var colorCounts = new List<ColorCount>();
            var sizeCountCollection = new List<SizeCount>();
            var shirts = new List<Shirt>();
            
            if (!options.Sizes.Any())
                options.Sizes = Size.All;
            
            foreach (var color in Color.All)
            {
                var shirtCountInSameColor = 0;

                var shirtsInSameColor = _groupByColorAndSize.FirstOrDefault(x => x.Color == color);

                if (shirtsInSameColor != null)
                {
                    foreach (var size in Size.All)
                    {
                        var shirtsInSameSize = shirtsInSameColor.SizeGroups.FirstOrDefault(g => g.Key == size);

                        if (shirtsInSameSize == null)
                            continue;
                        
                        if (options.Sizes.Contains(size))
                        {
                            shirtCountInSameColor += shirtsInSameSize.Count();
                        }

                        if (options.Colors.Contains(color))
                        {
                            sizeCountCollection.Add(new SizeCount
                            {
                                Size = size,
                                Count = shirtsInSameSize.Count()
                            });

                            if (options.Sizes.Contains(size))
                            {
                                shirts.AddRange(shirtsInSameSize);
                            }
                        }

                    }
                }

                colorCounts.Add(new ColorCount
                {
                    Color = color,
                    Count = shirtCountInSameColor,
                });
            }

            var sizeCounts = Size.All.Select(size =>
                    new SizeCount
                    {
                        Size = size,
                        Count = sizeCountCollection.Where(s => s.Size == size).Sum(s => s.Count)
                    })
                .ToList();

            return new SearchResults
            {
                Shirts = shirts,
                ColorCounts = colorCounts,
                SizeCounts = sizeCounts
            };
        }
    }
}